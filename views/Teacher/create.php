<?php
include_once ("../../vendor/autoload.php");
use App\Teacher\teacher;
$obj = new teacher();
$obj->setData($_GET);

$value = $obj->DepartmentView();
//echo "<pre>";
//print_r($value);
//die();
$value1 = $obj->DesignationView();

//if (!empty($_SESSION['user_info'])) {
//
?>
<?php include_once"../header.php"; ?>
<?php include_once("../Admin/side-menubar.php"); ?>

    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard
                    </h4>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                    <li><a style="color: #0a001f;" href="view.php">Teacher List</a></li>
                </ul>
            </div>
        </div>
        <!-- /page header -->
        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <!-- Basic layout-->
                    <form action="store.php" class="form-horizontal" method="post">
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h2 class="panel-title" > Add New Teacher<?php
                                        if (isset($_SESSION['message'])){
                                            ?>
                                    <button style="margin-left: 20px" class="btn btn-success" >
                                            <?php
                                            echo $_SESSION['message'];
                                            unset($_SESSION['message']);
                                        } ?></button></h2>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Name:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Teacher Name" name="title"required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Address:</label>
                                    <div class="col-lg-9">
                                        <textarea rows="7" cols="76" class="form-control" placeholder="Teacher Address" name="address"required></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Email:</label>
                                    <div class="col-lg-9">
                                        <input type="email" class="form-control" placeholder="Teacher Email" name="mail"required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Contact:</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" placeholder="Teacher Contact Number" name="contact"required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Department:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="dept_id" class="select">
                                            <optgroup label="Department">
                                                <?php
                                                foreach ($value as $item) {
                                                    ?>
                                                    <option value="<?php echo $item['id'];?>"><?php echo $item['title'];?></option>
                                                    <?php
                                                }
                                                ?>

                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Designation:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="designation_id" class="select">
                                            <optgroup label="Designation">
                                                <?php
                                                foreach ($value1 as $item1) {
                                                    ?>
                                                    <option value="<?php echo $item1['id'];?>"><?php echo $item1['title'];?></option>
                                                    <?php
                                                }
                                                ?>

                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Credit:</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" placeholder="Teacher Total Credit" name="credit"required>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">ADD<i class="icon-arrow-right14 position-right"></i></button>
                                </div>

                            </div>
                        </div>
                    </form>
                    <!-- /basic layout -->
                </div>

<?php
  include_once("../footer.php");
?>

<!--    		--><?php
//	} else{
//		$_SESSION['fail']= "You are not authorized!";
//		header('location:../../../index.php');
//	}

?>