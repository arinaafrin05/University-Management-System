<?php
include_once ("../../vendor/autoload.php");
use App\Course\course;
session_start();
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (($_POST['code'] != '') && ($_POST['title'] != '') &&($_POST['credit'] != '') && ($_POST['description'] != '')) {
//        if(!($_POST['code']<5)) {
            if (!($_POST['credit'] < 0.5 || $_POST['credit'] > 5)) {
                if (trim($_POST['code'])) {
                    $data = new course();
                    $data->setData($_POST)->store();
                } else {
                    $_SESSION['message'] = "Course Code must be unique";
                    header("location:create.php");
                }
            } else {
                $_SESSION['message'] = "Credit must be more than 0.5 and less than 5";
                header("location:create.php");
            }
//        }else{
//            $_SESSION['message'] = "Course Code must be more than 5";
//            header("location:create.php");
//        }
    }else {
        $_SESSION['message']="All Fields are required";
        header("location:create.php");
    }
}else {
    header("location:create.php");
}