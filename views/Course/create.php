<?php
include_once ("../../vendor/autoload.php");
use App\Course\course;
$obj = new course();
$obj->setData($_GET);

$value1 = $obj->SemesterView();

$value2 = $obj->DepartmentView();
$value3 = $obj->DepartmentView();
//echo "<pre>";
//print_r($value3);
//die();

//if (!empty($_SESSION['user_info'])) {
//
?>
                    <?php include_once"../header.php"; ?>


                     <?php include_once("../Admin/side-menubar.php"); ?>

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>

                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                    <li><a style="color: #0a001f;" href="view.php">Course List</a></li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-md-8">

                    <!-- Basic layout-->
                    <form action="store.php" class="form-horizontal" method="post">
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h2 class="panel-title" > Add New Course
                                    <?php
                                        if (isset($_SESSION['message'])){
                                            ?>
                                            <button style="margin-left: 20px" class="btn btn-success" >
                                            <?php
                                            echo $_SESSION['message'];
                                            unset($_SESSION['message']);
                                        } ?></button></h2>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Code:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Course Code" name="code">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Name:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Course Name" name="title">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Credit:</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" placeholder="Course Credit" name="credit">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Description:</label>
                                    <div class="col-lg-9">
                                        <textarea rows="8" cols="76" class="form-control"  name="description" placeholder="Course Description"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Department:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="dept_id" class="select">
                                            <optgroup label="Department">
                                                <?php
                                                foreach ($value2 as $item1) {
                                                    ?>
                                                    <option value="<?php echo $item1['id'];?>"><?php echo $item1['title'];?></option>
                                                    <?php
                                                }
                                                ?>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Semester:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="semester_id" class="select">
                                            <optgroup label="Semester">
                                                <?php
                                                foreach ($value1 as $item) {
                                                    ?>
                                                    <option value="<?php echo $item['id'];?>"><?php echo $item['title'];?></option>
                                                    <?php
                                                }
                                                ?>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">ADD<i class="icon-arrow-right14 position-right"></i></button>
                                </div>

                            </div>
                        </div>
                    </form>
                    <!-- /basic layout -->
                </div>

<?php
  include_once("../footer.php");
?>

<!--    		--><?php
//	} else{
//		$_SESSION['fail']= "You are not authorized!";
//		header('location:../../../index.php');
//	}

?>