<?php
include_once ("../../vendor/autoload.php");
use App\Department\department;
$obj = new department();
$obj->setData($_GET);
$value = $obj->show();
//echo "<pre>";
//print_r($value);
//die();
?>
<?php
//if (!empty($_SESSION['user_info'])) {
//?>
<?php include_once"../header.php"; ?>


<?php include_once("../Admin/side-menubar.php"); ?>



    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                    <li><a style="color: #0a001f;" href="create.php">Add New Department</a></li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <!-- Bordered striped table -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h4 class="panel-title">Department Table
                        <?php
                            if (isset($_SESSION['message'])){
                                ?>
                        <button style="margin-left: 20px" class="btn btn-success" >
                                <?php
                                echo $_SESSION['message'];
                                unset($_SESSION['message']);
                            } ?>
                        </button>
                    </h4>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>EDIT/DELETE</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($value as $item) {
                            ?>
                            <tr>
                                <td><?php echo $item['id']; ?></td>
                                <td><?php echo $item['code']; ?></td>
                                <td><?php echo $item['title']; ?></td>
                                <td>
                                    <a href="../Department/edit.php?id=<?php echo $item['id']; ?>" class="btn btn-success">Edit</a>
                                    <a href="../Department/delete.php?id=<?php echo $item['id']; ?>"  onclick="return confirm('Do You Want To Delete?')"  class="btn btn-danger">DELETE</a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /bordered striped table -->

        </div>
    </div>
    <!-- /main content -->

<?php
  include_once("../footer.php");
?>
<!--    --><?php
//	} else{
//		$_SESSION['fail']= "You are not authorized!";
//		header('location:../../../index.php');
//	}

?>