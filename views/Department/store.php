<?php
include_once ("../../vendor/autoload.php");
use App\Department\department;
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(!empty($_POST['code'])){
        if (!empty($_POST['title'])){
            $data = new department();
            $data->setData($_POST)->store();
        }else{
            $_SESSION['message'] = "Please Enter Department Name.";
            header("location:create.php");
        }
    }else{
        $_SESSION['message'] = "Please Enter Department Code.";
        header("location:create.php");
    }
}else {
    header("location:create.php");
}
