$(document).ready(function () {
    $("#dept_id").change(function () {
        var dept_id = $(this).val();
        $.ajax({
            url: 'getTeacher.php',
            type: 'post',
            data: {depart: dept_id},
            dataType: 'json',
            success: function (response) {
                console.log(response['teacher']);
                var len = response['teacher'].length;
                $("#teacher_id").empty();

                $("#teacher_id").append("<option value=''>" + 'Select Teacher' + "</option>");
                for (var i = 0; i < len; i++) {
                    var id = response['teacher'][i]['id'];
                    var title = response['teacher'][i]['title'];
                    $("#teacher_id").append("<option value='" + id + "'>" + title + "</option>");
                }

                var clen = response['course'].length;
                $("#course_code").empty();

                $("#course_code").append("<option value=''>" + 'Select Course Code' + "</option>");
                for (var i = 0; i < clen; i++) {
                    var id = response['course'][i]['id'];
                    var code = response['course'][i]['code'];
                    $("#course_code").append("<option value='" + id + "'>" + code + "</option>");
                }
            }
        });
    });
    $("#course_code").change(function () {
        var course_id = $(this).val();
        console.log(course_id);
        $.ajax({
            url: 'getTeacher.php',
            type: 'post',
            data: {course_id: course_id},
            dataType: 'json',
            success: function (response) {
                console.log(response);
                var title = response['title'];
                $("#course_name").empty();

                var credit = response['credit'];
                $("#course_credit_c").empty();

                $("#course_name").val(title);
                $("#course_credit_c").val(credit);
            }
        });
    });

    $("#teacher_id").change(function () {
        var teacher_id = $(this).val();
        $.ajax({
            url: 'getTeacher.php',
            type: 'post',
            data: {teacher_id: teacher_id},
            dataType: 'json',
            success: function (response) {
                console.log(response['teacher']['credit']);
                var credit = response['teacher']['credit'];
                $("#total_credit").empty();
                console.log(response['a_credit']['']);
                var assign = response['a_credit']['assign_credit'];
                if (assign > 0) {
                   var  remaining = credit - assign;
                } else {
                   remaining = credit;
                }
                // console.log(assign);
                $("#total_credit").val(credit);
                $("#remain_credit").val(remaining);
            }
        });
    });

    //View Course Static

    $("#department_id").change(function () {
        var dept_id = $(this).val();
        console.log(dept_id);
        $.ajax({
            url: 'getTeacher.php',
            type: 'post',
            data: {dept_id: dept_id},
            dataType: 'json',
            success: function (response) {
                console.log(response['courses']);
                console.log(response['teacher_title']);

                $("#table").empty();
                var len = response['courses'].length && response['teacher_title'].length;
                $("#view").show(100);
                if(len>0){
                        for (var i = 0; i < len; i++) {
                            var id = response['courses'][i]['id'];
                            var code = response['courses'][i]['code'];
                            var title = response['courses'][i]['title'];
                            var semester = response['courses'][i]['semester'];
                            var teacher = response['teacher_title'][i]['title'];

                            $("#table").append("<tr>");
                            $("#table").append("<td>" + code + "</td>");
                            $("#table").append("<td>" + title + "</td>");
                            $("#table").append("<td>" + semester + "</td>");
                            $("#table").append("<td>" + teacher + "</td>");

                            $("#table").append("</tr>");
                        }
                }
            }
        });
    });

});