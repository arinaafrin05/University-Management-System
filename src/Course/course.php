<?php

namespace App\Course;
use PDO;
class course
{
    private $id = "";
    private $dept_id = "";
    private $semester_id = "";
    private $code = "";
    private $title = "";
    private $credit = "";
    private $description = "";
    private $department = "";
    private $semester = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=varsitysystem', 'root', '');

    }
    public function setData($data='')
    {
        if (array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if (array_key_exists('dept_id',$data))
        {
            $this->dept_id = $data['dept_id'];
        }
        if (array_key_exists('semester_id',$data))
        {
            $this->semester_id = $data['semester_id'];
        }
        if(array_key_exists('code',$data)){
            $this->code = $data['code'];
        }
        if(array_key_exists('title',$data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('credit',$data)){
            $this->credit = $data['credit'];
        }
        if(array_key_exists('description',$data)){
            $this->description = $data['description'];
        }
        if(array_key_exists('department',$data)){
            $this->department = $data['department'];
        }
        if(array_key_exists('semester',$data)){
            $this->semester = $data['semester'];
        }
        return $this;
    }
    public function SemesterView()
    {
        try{
            $query = "SELECT * FROM `semesters` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $semester = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $semester;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function DepartmentView()
    {
        try{
            $query = "SELECT * FROM `departments` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $dept = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $dept;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function store()
    {
        try {
            $query = "INSERT INTO `courses` (`id`, `dept_id`,`semester_id`,`code`, `title`,`credit`,`description`,`created_at`) VALUES (:id,:dept_id,:semester_id,:code,:title,:credit,:description,:create)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':dept_id' => $this->dept_id,
                    ':semester_id' => $this->semester_id,
                    ':code'=>$this->code,
                    ':title' => $this->title,
                    ':credit' => $this->credit,
                    ':description' => $this->description,
                    ':create'=> date('Y-m-d h:m:s'),
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Stored";
                header("location:create.php");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }
    public function show()
    {
        try{
            $query = "SELECT courses.id as id,courses.credit as credit,courses.code as code,courses.title as title,courses.description as description,semesters.title as semester_title,departments.title as dept_title FROM `courses` LEFT JOIN `semesters` ON courses.semester_id = semesters.id LEFT JOIN `departments` ON courses.dept_id=departments.id";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function view()
    {
        try{
            $query = "SELECT courses.id as id,courses.code as code,courses.credit as credit, courses.title as title,courses.description as description,departments.id as dept_id ,departments.title as dept_title FROM `courses`,`departments` where courses.id="."'".$this->id."'" ." AND courses.dept_id=departments.id";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch(PDO::FETCH_ASSOC);

            $query1 = "SELECT courses.id as id,semesters.id as semester_id,semesters.title as semester_title FROM `courses`,`semesters` where courses.id="."'".$this->id."'" ." AND courses.semester_id=semesters.id";
            $stmnt = $this->pdo->prepare($query1);
            $stmnt->execute();
            $value1 = $stmnt->fetch(PDO::FETCH_ASSOC);
            $data= [
                'department'=>$value,
                'semester'=>$value1,
            ];
            return $data;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function update()
    {
//        print_r($_POST);
//        die();
        try {
            $query = "UPDATE `courses` SET `dept_id`=:dept,`semester_id`=:semester_id,`code`=:code,`title`=:title,`credit`=:credit,`description`=:description,`updated_at`=:update WHERE id = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(
                ':id' => $this->id,
                ':dept' => $this->dept_id,
                ':semester_id' => $this->semester_id,
                ':code'=>$this->code,
                ':title'=>$this->title,
                ':credit'=>$this->credit,
                ':description'=>$this->description,
                ':update'=>date('Y-m-d h:m:s'),
            ));
            if ($stmt) {
                $_SESSION['update-message'] = "Successfully Data Updated";
                header("location:edit.php?id=$this->id");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $query = "DELETE FROM courses WHERE id = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->bindParam(':id', $this->id);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Course/view.php");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}